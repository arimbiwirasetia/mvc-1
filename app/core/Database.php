<?php 
    class Database {
        private $host = DB_HOST;
        private $user = DB_USER;
        private $pass = DB_PASS;
        private $db_name = DB_NAME;
        private $dbh;
        private $stmt;
    
    public function __construct() {
        //data source name
        $dsn = "mysql:host={$this->host};dbname={$this->db_name}";
        $option = [
            PDO :: ATTR_PERSISTENT => true, //
            PDO :: ATTR_ERRMODE => PDO :: ERRMODE_EXCEPTION  //membuat errot utk yg benar"error
        ];
        try {
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $option);
        } catch (PDOException $e) { // PDO = class khusus
            die($e->getMessage());
        }
    }

    public function query ($query) { //memanggil prepare dr PDO => baru menyapkan, isisnya bisa blkngan - sgt baik utk mvc
        $this->stmt = $this->dbh->prepare($query);
    }

    public function bind($param, $value, $type=null) {
        if (is_null($type)) { //cttn;paper
            switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;    //bisa bnr" memasukan null drpd kosong
                    break;
                default:
                    $type = PDO::PARAM_STR;
                    break;
            }
        }
        $this->stmt->bindValue($param, $value, $type);
    }

    public function execute() { //baru eksekusi trhdp query -belum ada hasil
        $this->stmt->execute();
        // return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function resultAll(){    //baru ada hasil di result
        $this->execute();
        return $this->stmt->fetchAll(PDO::FETCH_ASSOC); //fetch assoc = mengubah jadi assoc array
    }

    public function resultSingle(){    //fetch single = line pertama
        $this->execute();
        return $this->stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function rowCount() {
        return $this->stmt->rowCount();
    }
}