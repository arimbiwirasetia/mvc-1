<?php

class Register extends Controller
{
  public function index()
  {
    $data['judul'] = 'Register';
    $this->view('templates/header', $data);
    $this->view('register/index');
    $this->view('templates/footer');
  }

  public function registerAdd()
  {
    if ($_POST['password'] == $_POST['confirm_password']) {
      if ($this->model('User_model')->getUserByEmail($_POST['email'])) {
        Flasher::setFlash('Akun sudah', 'terdaftar', 'success');
        header('Location: ' . BASE_URL . '/register');
        exit;
      } else {
        if ($this->model('User_model')->tambahDataUser($_POST) > 0){
          FLasher ::setFlash('Akun berhasil', 'terdafatar', 'success');
          header('Location: '. BASE_URL . '/login');
          exit;
        } else {
          FLasher::setFlash('Akun gagal'.'terdaftar','danger');
          header('Location: ' . BASE_URL . '/register');
        }
      }
    }
  }
}