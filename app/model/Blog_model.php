<?php 
    class Blog_model {
        private $table = 'blog';
        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function getAllBlog() {
            $this->db->query("SELECT * FROM {$this->table}");
            return $this->db->resultAll();
        }

        public function getBlogById($id) {
            $this->db->query("SELECT * FROM {$this->table} WHERE id=:id");
            
            $this->db->bind("id", $id);
            return $this->db->resultSingle();
        }

        public function addDataBlog($data) {
            // $this->db = query("INSERT INTO {$this->table} VALUES ('', :penulis, :judul, :tulisan)");
            $query = "INSERT INTO blog VALUES (null, :penulis, :judul, :tulisan)";


            $this->db->query($query);
            $this->db->bind('penulis', $data['penulis']);
            $this->db->bind('judul', $data['judul']);
            $this->db->bind('tulisan', $data['tulisan']);

            $this->db->execute();

            return $this->db->rowCount();
        }

        public function deleteDataBlog($id) {
            $query = "DELETE FROM blog WHERE id = :id";
            
            $this->db->query($query);
            $this->db->bind('id', $id);

            $this->db->execute();

            return $this->db->rowCount();
        }


}