<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman<?= $data["judul"]; ?></title>
    <link rel="stylesheet" href="	<?=BASE_URL;?>/css/bootstrap.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
  </head>
  <body>
  <nav class="navbar navbar-expand-lg mb-5" style="background-color: #f9b4ed;">
    <div class="container-fluid">
      <a class="navbar-brand ms-5 me-5 pe-5" href="<?=BASE_URL;?>">MVC</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item me-4">
            <a class="nav-link" aria-current="page" href="<?=BASE_URL;?>">Home</a>
          </li>
          <li class="nav-item me-4">
            <a class="nav-link" aria-current="page" href="<?=BASE_URL;?>/user/profile">Profile</a>
          </li>
          <li class="nav-item me-4 justify-content-between">
            <a class="nav-link " href="<?=BASE_URL;?>/blog">Blog</a>
          </li>
          <li class="nav-item me-4">
            <a class="nav-link" href="<?=BASE_URL;?>/user">User</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?=BASE_URL;?>/register">Register</a>
          </li>
        </ul>
        <form class="d-flex" role="search">
          <input class="form-control" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-dark ms-2" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>

  
