<div class="container d-flex justify-content-center my-auto">
      <div class="row w-50">
        <form class="rounded p-5">
          <div class="mb-3">
            <label for="InputEmail" class="form-label">Username</label><br />
            <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Masukkan Username" />
          </div>
          <div class="mb-3">
            <label for="InputPassword" class="form-label">Password</label><br />
            <input type="password" class="form-control" id="InputPassword" placeholder="Masukkan Password" />
          </div>
          <div class="d-flex justify-content-between mt2">
            <!-- <button type="button" class="btn btn-secondary">Create account</button> -->
            <button class="btn btn-secondary"><a href="register.html" class="text-decoration-none text-light">Create account</a></button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>

