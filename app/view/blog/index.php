<div class="container">

  <div class="row">
    <div class="col-lg-6">
      <?php Flasher::flash(); ?>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      <button type="button" class="btn btn-primary mb-3 tombolAddBlog" data-bs-toggle="modal" data-bs-target="#AddBlog">Add New Blog</button>      
    </div>
  </div>

  <div class="row">
    <div class="col-lg-6">
      
      <h3>Blog</h3>
      <ul class="list-group">

        <?php foreach($data["blog"] as $blog) : ?>
          <li class="list-group-item">
            <?= $blog['judul']; ?>

            <a href="<?= BASE_URL; ?>/blog/delete/<?= $blog['id'];?>" class="badge fw-normal text-bg-danger text-decoration-none float-end" onclick="return confirm('yakin?');">delete</a>
            
            <a href="<?= BASE_URL; ?>/blog/edit/<?= $blog['id'];?>" class="badge fw-normal text-bg-success text-decoration-none float-end me-1 tampilModalEdit" data-bs-toggle="modal" data-bs-target="#AddBlog" data-id="<?= $blog['id']?>">edit</a>

            <a href="<?= BASE_URL; ?>/blog/detail/<?= $blog['id'];?>" class="badge fw-normal text-bg-primary text-decoration-none float-end me-1">read</a>

          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>

<!-- Add Blog Modal -->
<div class="modal fade" id="AddBlog" tabindex="1" aria-labelledby="judulModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModalLabel">Add New Blog</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">        </button>
      </div>

      <div class="modal-body">
        <form action="<?= BASE_URL; ?>/blog/Add" method="post">
        <div class="form-group mb-2">
          <label for="penulis" class="form-label">Penulis</label>
          <input type="text" class="form-control" id="penulis" name="penulis" placeholder="Enter name here" />
        </div>
        
        <div class="form-group mb-3">
          <label for="judul" class="form-label">Judul</label>
          <input type="text" class="form-control" id="judul" name="judul" placeholder="Enter title here" />
        </div>

        <div class="form-group mb-3">
          <label for="tulisan" class="form-label">Tulisan</label>
          <textarea type="text" class="form-control" id="tulisan" name="tulisan" placeholder="Enter content here" rows="3"> </textarea>
        </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Add</button>
        </form>
      </div>
    </div>
  </div>
</div>