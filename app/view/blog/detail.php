<div class="container mt-5">
  <div class="card" style="width: 18rem">
    <div class="card-body">
      <h5 class="card-title"><?= $data['blog']['judul'];?></h5>
      <p class="card-text"><?= $data['blog']['penulis'];?></p>
      <a href="#" class="btn btn-primary"><?= $data['blog']['tulisan'];?></a>
    </div>
  </div>
</div>
