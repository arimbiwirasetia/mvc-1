<div class="container d-flex justify-content-center align-content-center">
      <div class="row align-self-center w-50">
        <form class="rounded p-5">
          <div class="mb-3">
            <label for="InputUsername" class="form-label">Username</label><br />
            <input type="text" class="form-control" id="InputUsername" aria-describedby="usernameHelp" placeholder="Masukkan Username" />
          </div>
          <div class="mb-3">
            <label for="InputEmail" class="form-label">Email</label><br />
            <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Masukkan Email" />
          </div>
          <div class="mb-3">
            <label for="InputPassword" class="form-label">Password</label><br />
            <input type="password" class="form-control" id="InputPassword" placeholder="Masukkan Password" />
          </div>
          <div class="mb-3">
            <label for="InputRePassword" class="form-label">Password</label><br />
            <input type="password" class="form-control" id="InputRePassword" placeholder="Masukkan Password" />
          </div>
          <div class="d-flex justify-content-between mt-4">
            <button class="btn btn-secondary"><a href="login.html" class="text-decoration-none text-light">Already have account</a></button>
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </form>
      </div>
    </div>