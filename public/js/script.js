$(function () {
  $(".tombolAddBlog").on("click", function () {
    $("#judulModalLabel").html("Add New Blog");
    $(".modal-footer button[type=submit]").html("Add New Blog");
  });

  $(".tampilModalEdit").on("click", function () {
    $("#judulModalLabel").html("Edit Blog");
    $(".modal-footer button[type=submit]").html("Edit Blog");
  });
});
